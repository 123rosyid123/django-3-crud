from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from pegawai.forms import PegawaiForm  
from pegawai.models import Pegawai  

# Create your views here.
def show(request):  
    pegawais = Pegawai.objects.all()  
    return render(request,"show.html",{'pegawais':pegawais})  

@login_required
def tambah(request):
    if request.method == "POST":  
        form = PegawaiForm(request.POST)  
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('list')  
            except:  
                pass  
    else:  
        form = PegawaiForm()  
    return render(request,'tambah.html',{'form':form})  

@login_required
def edit(request, id):  
    pegawai = Pegawai.objects.get(id=id)  
    return render(request,'edit.html', {'pegawai':pegawai})  

@login_required
def update(request, id):  
    pegawai = Pegawai.objects.get(id=id)  
    form = PegawaiForm(request.POST, instance = pegawai)
    if form.is_valid():  
        form.save()  
        return redirect("list")  
    print(form.errors)
    return render(request, 'edit.html', {'pegawai': pegawai})  

@login_required
def destroy(request, id):  
    pegawai = Pegawai.objects.get(id=id)  
    pegawai.delete()  
    return redirect("list")  