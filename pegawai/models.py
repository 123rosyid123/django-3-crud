from django.db import models

# Create your models here.
class Pegawai(models.Model):  
    pid = models.CharField(max_length=20)  
    pnama = models.CharField(max_length=100)  
    pemail = models.EmailField()  
    pkontak = models.CharField(max_length=15)  
    class Meta:  
        db_table = "Pegawai"