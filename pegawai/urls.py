# from django.contrib import admin  
from django.urls import path  
from . import views
urlpatterns = [  
    path('',views.show, name='list'),
    path('tambah', views.tambah, name='tambah'),  
    path('edit/<int:id>', views.edit, name='edit'),
    path('update/<int:id>', views.update, name='update'),
    path('delete/<int:id>', views.destroy, name='delete'),
]  